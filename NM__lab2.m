f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));

left_point = -2; 
right_point = 2;
N = 8;
step = (right_point-left_point)/(N-1); 

X = left_point:step:right_point;
Y = f(X);

S = zeros(N);
c = zeros(1,3);
h = zeros(1,N-1);
coefs = zeros(N-1,4);
coefs (:,4) = Y(1:end-1);

for i=1:N-1
    h(i) = X(i+1) - X(i);
end

for i=2:N-1
    S(i,i-1)=h(i-1);
    S(i,i)=2*(h(i-1)+h(i));
    S(i,i+1)=h(i);
    c(i)=(Y(i+1)-Y(i))./h(i)-(Y(i)-Y(i-1))./h(i-1);
end

S(1,1)=-6;
S(1,2)=6;
S(N,N-1)=-6;
S(N,N)=6;
c(1) = 0;
c(N) = 0;

c=transpose(c);

sigma=S\c;

for i=1:N-1
    coefs(i,3) = (Y(i+1)-Y(i))./h(i)-h(i).*(sigma(i+1)+2.*sigma(i));
    coefs(i,2) = 3.*sigma(i);
    coefs(i,1) = (sigma(i+1)-sigma(i))./h(i);
end

e_max = 0.3446;

figure(1);

fplot(f,[left_point right_point]);
hold on

for i=1:N-1
   fplot(@(x) polyval(coefs(i,:),x-X(i)),[X(i) X(i+1)],color='red');
end

scatter(X,Y,20,'o','filled',"black");

grid
legend('original function','spline interpolation');
hold off

figure(2);

fplot(e_max, [left_point right_point]);

hold on

for i=1:N-1
   fplot(@(x) abs(f(x)-polyval(coefs(i,:),x-X(i))),[X(i) X(i+1)],color='red');
end

ylim([0 e_max*1.1]);
grid
legend('max error','absolute error');
hold off